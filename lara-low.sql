-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 18, 2018 at 10:11 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 5.6.34-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lara-low`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_us` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to_us` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `from_us`, `to_us`, `content`, `created_at`, `updated_at`) VALUES
(5, 'h515_20180413073457', 'ORYl_20180413073633', 'Cm85HxZmZgJMUEycSGEMSSBz/YAGdUYEI1og0V0LQPMrrp7qUsG5cnRC89qapZ3VjowEbKriTNVmHhxwqBW1spDkWF2t/+MAE/6yn/Ct8Yc42HQLMU1AogmA/iTwGBHwXfatX1fnppAUoU6CCq5/xCX2+BiusFdf1qtAxy+XtAEmUcD2kIchrR3g3zfFXjVP', '2018-04-16 01:04:11', NULL),
(6, 'h515_20180413073457', 'RvSq_20180413073516', 'Cm85HxZmZgJMUEycSGEMSSBz/YAGdUYEI1og0V0LQPMrrp7qUsG5cnRC89qapZ3VjowEbKriTNVmHhxwqBW1spDkWF2t/+MAE/6yn/Ct8Yc42HQLMU1AogmA/iTwGBHwXfatX1fnppAUoU6CCq5/xCX2+BiusFdf1qtAxy+XtAEmUcD2kIchrR3g3zfFXjVP', '2018-04-16 01:04:12', NULL),
(7, 'h515_20180413073457', 'ORYl_20180413073633', 'Cm85HxZmZgJMUEycSGEMSSBz/YAGdUYEI1og0V0LQPMrrp7qUsG5cnRC89qapZ3VjowEbKriTNVmHhxwqBW1spDkWF2t/+MAE/6yn/Ct8Yc42HQLMU1AogmA/iTwGBHwXfatX1fnppAUoU6CCq5/xCX2+BiusFdf1qtAxy+XtAEmUcD2kIchrR3g3zfFXjVP', '2018-04-16 01:04:15', NULL),
(8, 'ORYl_20180413073633', 'h515_20180413073457', 'Cm85HxZmZgJMUEycSGEMSSBz/YAGdUYEI1og0V0LQPMrrp7qUsG5cnRC89qapZ3VjowEbKriTNVmHhxwqBW1spDkWF2t/+MAE/6yn/Ct8Yc42HQLMU1AogmA/iTwGBHwXfatX1fnppAUoU6CCq5/xCX2+BiusFdf1qtAxy+XtAEmUcD2kIchrR3g3zfFXjVP', '2018-04-16 01:04:14', NULL),
(9, 'h515_20180413073457', 'ORYl_20180413073633', 'Cm85HxZmZgJMUEycSGEMSSBz/YAGdUYEI1og0V0LQPMrrp7qUsG5cnRC89qapZ3VjowEbKriTNVmHhxwqBW1spDkWF2t/+MAE/6yn/Ct8Yc42HQLMU1AogmA/iTwGBHwXfatX1fnppAUoU6CCq5/xCX2+BiusFdf1qtAxy+XtAEmUcD2kIchrR3g3zfFXjVP', '2018-04-16 01:04:16', NULL),
(10, 'h515_20180413073457', 'ryNI_20180413073840', 'h6rhJpV9BcaUcmOFOmK5QjBpkhq6OGumj5lo3Z2Jkcy0y/bN8LDiBEqVaY9yop45', '2018-04-16 07:04:42', NULL),
(11, 'ryNI_20180413073840', 'h515_20180413073457', 'DNeuyXv4cZYKBmiB5jtVdvEv+bp8/+He9yRHcFe1HHY2vjPeE0W7oCLxs7h+2xY2WzmYsc6ytMLwTdGcx0x5CQ==', '2018-04-16 07:04:43', NULL),
(12, 'h515_20180413073457', 'ryNI_20180413073840', 'Wm2n4uX7Ab9lg7me7kc6aHRxUNKdbRbYFTQUh+yVv4k5Y41Ow1OHQh2Ogoh/CSJX', '2018-04-16 08:04:25', NULL),
(13, 'h515_20180413073457', 'ryNI_20180413073840', 'nZH8NIQlhLqpikgTgEKp/QX6TlCL5vD4zKXlQAr6Ik8S8U/CkgsIbr/pLPFR444Z', '2018-04-16 09:04:59', NULL),
(14, 'h515_20180413073457', 'ryNI_20180413073840', '4VCn4vQ5OxJTPhCT14zYOGgUin7NtZoYFaf9LSljpRRXnu5o901AEMq3/d7Gy47l', '2018-04-16 10:04:00', NULL),
(15, 'ryNI_20180413073840', 'ryNI_20180413073840', 'bUStNKYcNnKWEO3bZWKSUkFquoO36QQKzFbRYhb0XLM=', '2018-04-16 10:04:09', NULL),
(16, 'ryNI_20180413073840', 'ryNI_20180413073840', 'pIQ6UqLpqb7zb4PhHEXRh0pQJcwjSyPKapO9jFzSflQ=', '2018-04-16 10:04:09', NULL),
(17, 'ryNI_20180413073840', 'h515_20180413073457', 'k/I2C4XwXFFJQBvVqQvnzdma1QKXDWqL+hH9Rb14aVs=', '2018-04-16 10:04:11', NULL),
(18, 'h515_20180413073457', 'ryNI_20180413073840', '0cClBBwgyMl83h+86a85CFXrOLXgB7LE4LC1mRuGvkQ=', '2018-04-16 10:04:12', NULL),
(19, 'h515_20180413073457', 'ryNI_20180413073840', '3bm01SXAnxo6rrznZHvLjWX8fDDM2d4iRz3z4E6prUk=', '2018-04-16 10:04:14', NULL),
(20, 'h515_20180413073457', 'ryNI_20180413073840', 'fUcy7nVwSXFUKaeKq8hq7fbctAp8i6AFWsR6uOtG1P4=', '2018-04-16 10:04:15', NULL),
(21, 'h515_20180413073457', 'ryNI_20180413073840', 'huV6lwfA4jCaIpCA5uHD/YVSu8vwb2ufmKxDLLSTAk4=', '2018-04-16 10:04:16', NULL),
(22, 'h515_20180413073457', 'ryNI_20180413073840', 'ZxKIaJRANqXJxFxXcDKu5ZotDn0miYD0HpD9cj2sDsg=', '2018-04-16 10:04:16', NULL),
(23, 'h515_20180413073457', 'ryNI_20180413073840', 'inOkpOy+KVS2aHCFzE70TLtdCIKbfG3qY1W/agLeXsY=', '2018-04-16 10:04:24', NULL),
(24, 'h515_20180413073457', 'ryNI_20180413073840', 'jMdQP2A+tveodsEDrPumVDxMzosK87R9tFksflfi7+o=', '2018-04-16 10:04:27', NULL),
(25, 'h515_20180413073457', 'ryNI_20180413073840', 'PB0+5pHJQuzcg8KIy1oXRp7JE4GTZ+SdWUrfLa19Fuk=', '2018-04-16 10:04:27', NULL),
(26, 'h515_20180413073457', 'ryNI_20180413073840', 'Z+P8IVc0zxfTkV/b5+ga5P7mQdXWg2LOLW0XeXon7X4=', '2018-04-16 10:04:27', NULL),
(27, 'ryNI_20180413073840', 'h515_20180413073457', 'aB+fg+KBRfruercCnTQ+FOJlu6lb5U62Ho/WtCm0kE4=', '2018-04-16 10:04:28', NULL),
(28, 'ryNI_20180413073840', 'h515_20180413073457', 'sD+7m9bXnrR/aeNolVEIuTjoyzfSdi+F5LYDdYd9CWg=', '2018-04-16 10:04:29', NULL),
(29, 'h515_20180413073457', 'ryNI_20180413073840', 'x/wqDpMLbhAo7Mks/E9z+k3n7PtKYiotkkW/yGTKacM=', '2018-04-16 10:04:35', NULL),
(30, 'h515_20180413073457', 'ryNI_20180413073840', 'Xmb4mEY4Tp1wCSXz2LCXxQXP6GFVecdWBtEwOJ8Kq/M=', '2018-04-17 04:04:01', NULL),
(31, 'h515_20180413073457', 'ryNI_20180413073840', 'EOtnNk3N3UlRCbHraW4s1r44UybvVpFDAWgcDkjT+nU=', '2018-04-17 07:04:08', NULL),
(32, 'h515_20180413073457', 'ryNI_20180413073840', 'T3lMd9jromuYDDM4AXHhdRgPT8lobFyFnhU6s/hR3jQ=', '2018-04-17 07:04:08', NULL),
(33, 'h515_20180413073457', 'ryNI_20180413073840', 'Y+ge7dcyhqDjt8KaZrnoeXieS57SiJPwfiwx/Ep4r2Q=', '2018-04-17 07:04:42', NULL),
(34, 'h515_20180413073457', 'ryNI_20180413073840', 'htxNDIzSvhX40nGNOWRaAVPnwPSNwdYyT/nixbwrqVA=', '2018-04-17 07:04:44', NULL),
(35, 'h515_20180413073457', 'ryNI_20180413073840', 'CX69CBrU6Cz8tqVb0eyXNjEAoHQQ0TBb3qtLPDi//x4=', '2018-04-17 07:04:44', NULL),
(36, 'h515_20180413073457', 'ryNI_20180413073840', 'nR7P/XVMN5GC9D0pfv2EAUEFeXorfhOQ5kOaTO/i6+g=', '2018-04-17 10:04:20', NULL),
(37, 'h515_20180413073457', 'ryNI_20180413073840', 'Wn55WY9mRhVaQUhJjbCHdgcWdr9eAlSbIiTb+ejr36o=', '2018-04-17 10:04:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2018_04_04_071845_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(10) NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `user_id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(13, 'h515_20180413073457', 'Typically, this is done after successfully performing an action when you flash a succes', 'Redirecting to a new URL and flashing data to the session are usually done at the same time. Typically, this is done after successfully performing an action when you flash a success message to the session. For convenience, you may create a RedirectResponse instance and flash data to the session in a single, fluent method chain:\r\n\r\nRoute::post(\'user/profile\', function () {\r\n    // Update the user\'s profile...\r\n\r\n    return redirect(\'dashboard\')->with(\'status\', \'Profile updated!\');\r\n});\r\n\r\nAfter the user is redirected, you may display the flashed message from the session. For example, using Blade syntax:', '2018-04-13 07:41:32', NULL),
(14, 'h515_20180413073457', 'Introduction', 'The database query builder provides a convenient, fluent interface to creating and running database queries. It can be used to perform most database operations in your application, and works on all supported database systems.\r\n\r\n', '2018-04-16 07:33:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` int(10) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(10) DEFAULT '1',
  `level` int(10) DEFAULT '2',
  `friends` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `date_of_birth`, `address`, `phone`, `gender`, `created_at`, `updated_at`, `status`, `level`, `friends`) VALUES
('4', 'Admin', 'matsunaga@bravo-global.co', '$2y$10$6aeMvsEdaJfofxLbue/py.5hi01EVQQtjlKLGf0DSaQqAgtLj8xza', '2018-01-28', '', '', 1, '2018-04-12 01:39:11', NULL, 1, 0, NULL),
('48T4_20180413073814', 'Trần Văn AT', 't@email.com', '$2y$10$6aeMvsEdaJfofxLbue/py.5hi01EVQQtjlKLGf0DSaQqAgtLj8xza', '2018-03-25', '', '', 1, '2018-04-13 07:38:14', NULL, 1, 2, NULL),
('EPZu_20180413073344', 'nguyen van minh', 'minh.nv@neo-lab.vn', '$2y$10$6aeMvsEdaJfofxLbue/py.5hi01EVQQtjlKLGf0DSaQqAgtLj8xza', '2017-12-31', 'Quảng nam', '', 1, '2018-04-13 07:33:44', NULL, 1, 2, NULL),
('h515_20180413073457', 'Trần Văn A', 'a@email.com', '$2y$10$6aeMvsEdaJfofxLbue/py.5hi01EVQQtjlKLGf0DSaQqAgtLj8xza', '2000-12-23', '', '', 1, '2018-04-13 07:34:58', NULL, 1, 2, '{"friends":{"1":{"id":"ORYl_20180413073633"},"2":{"id":"ryNI_20180413073840"}}}'),
('ORYl_20180413073633', 'Trần Văn F', 'f@email.com', '$2y$10$6aeMvsEdaJfofxLbue/py.5hi01EVQQtjlKLGf0DSaQqAgtLj8xza', '2017-11-26', '', '', 1, '2018-04-13 07:36:33', NULL, 1, 2, NULL),
('RvSq_20180413073516', 'Trần Văn B', 'b@email.com', '$2y$10$6aeMvsEdaJfofxLbue/py.5hi01EVQQtjlKLGf0DSaQqAgtLj8xza', '2017-09-24', '', '', 1, '2018-04-13 07:35:16', NULL, 1, 2, NULL),
('ryNI_20180413073840', 'Trần Văn U', 'u@email.com', '$2y$10$6aeMvsEdaJfofxLbue/py.5hi01EVQQtjlKLGf0DSaQqAgtLj8xza', '2018-03-25', '', '', 1, '2018-04-13 07:38:40', NULL, 1, 2, '{"friends":{"1":{"id":"h515_20180413073457"},"2":{"id":"ORYl_20180413073633"}}}'),
('tDhB_20180413073735', 'Trần Văn G', 'g@email.com', '$2y$10$6aeMvsEdaJfofxLbue/py.5hi01EVQQtjlKLGf0DSaQqAgtLj8xza', '2018-02-05', '', '', 1, '2018-04-13 07:37:35', NULL, 1, 2, NULL),
('TtWI_20180413073551', 'Trần Văn D', 'd@email.com', '$2y$10$6aeMvsEdaJfofxLbue/py.5hi01EVQQtjlKLGf0DSaQqAgtLj8xza', '2017-12-31', '', '', 1, '2018-04-13 07:35:51', NULL, 1, 2, NULL),
('ujcy_20180413073534', 'Trần Văn C', 'c@email.com', '$2y$10$6aeMvsEdaJfofxLbue/py.5hi01EVQQtjlKLGf0DSaQqAgtLj8xza', '2017-09-24', 'Quảng nam', '', -1, '2018-04-13 07:35:34', NULL, 1, 2, NULL),
('YKZx_20180413073650', 'Trần Văn E', 'e@email.com', '$2y$10$6aeMvsEdaJfofxLbue/py.5hi01EVQQtjlKLGf0DSaQqAgtLj8xza', '2017-11-26', '', '', 1, '2018-04-13 07:36:50', NULL, 1, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_images`
--

CREATE TABLE `user_images` (
  `id` int(10) NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_images`
--

INSERT INTO `user_images` (`id`, `user_id`, `url`) VALUES
(24, '4', 'SpJhjF23b75EaORK_20180412013911.jpg'),
(31, 'EPZu_20180413073344', 'hhsvJNPMDecpqCni_20180413073344.jpg'),
(32, 'h515_20180413073457', 'ZkSmD7NWm19Cc9EP_20180413073458.jpg'),
(33, 'RvSq_20180413073516', '9BNVxKIturf7gEde_20180413073516.jpg'),
(34, 'ujcy_20180413073534', 'YtN5PW6DyYLbNUtP_20180413073534.jpg'),
(35, 'TtWI_20180413073551', 'Ebx9PNofFwrIrBde_20180413073551.jpg'),
(36, 'ORYl_20180413073633', 'd1iMyVHJfKg1nxbS_20180413073633.jpg'),
(37, 'YKZx_20180413073650', 'L70tIsadq0S6oiL3_20180413073650.jpg'),
(38, 'tDhB_20180413073735', '8jTpEMCoevnQsRES_20180413073735.jpg'),
(39, '48T4_20180413073814', 'QTXHLJ1FveX6XRAE_20180413073814.png'),
(40, 'ryNI_20180413073840', 'NCgWjs3MtaP2eZoq_20180413073840.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_ibfk_1` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_images`
--
ALTER TABLE `user_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_images_ibfk_1` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `user_images`
--
ALTER TABLE `user_images`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_images`
--
ALTER TABLE `user_images`
  ADD CONSTRAINT `user_images_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
