function  showImgBeforeUpload(input){
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
		$('#img-show')
		.attr('src', e.target.result);
		};
		reader.readAsDataURL(input.files[0]);
	} 
}