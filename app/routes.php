<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::filter("isAdmin", function()
{
    if (isset(Auth::user()->level) && Auth::user()->level != 0) return Redirect::to('/login');
});


Route::get('/test',                ['as' => 'test.index',          'uses' => 'TestController@index']);
Route::get('/viewtoken',            ['as' => 'test.index',          'uses' => 'TestController@viewToken']);

Route::get('/',                     ['as' => 'home.index',          'uses' => 'HomeController@index']);
Route::get('/posts',                ['as' => 'home.index',          'uses' => 'HomeController@index']);
Route::get('/post/{id}',            ['as' => 'post.show',           'uses' => 'PostController@show']);

Route::get('/login',                ['as' => 'home.login',          'uses' => 'HomeController@login']);
Route::post('/execLogin',           ['as' => 'home.execLogin',      'uses' => 'HomeController@execLogin']);
Route::get('/logout',               ['as' => 'home.logout',         'uses' => 'HomeController@logout']);

// Before user login
Route::group(array('prefix' => 'user', 'namespace' => 'user'), function()
{
    Route::get('/register',         ['as' => 'user.register',       'uses' => 'UserController@register']);
    Route::post('/execregister',    ['as' => 'user.exec_register',  'uses' => 'UserController@execRegister']);
    Route::get('/register/{code}',  ['as' => 'user.register_confirm','uses' => 'UserController@registerConfirm']);
});

// User group
Route::group(array('prefix' => 'user', 'before' => 'auth',  'namespace' => 'user'), function ()
{
    Route::get('/',                 ['as' => 'user.index',          'uses' => 'UserController@index']);
    Route::get('/mypost',           ['as' => 'user.mypost',         'uses' => 'UserController@myPost']);
    Route::get('/createpost',       ['as' => 'user.create_post',    'uses' => 'UserController@createPost']);
    Route::post('/storepost',       ['as' => 'user.store_post',     'uses' => 'UserController@storePost']);
    Route::get('/editpost/{id}',    ['as' => 'user.edit_post',      'uses' => 'UserController@editPost']);
    Route::post('/updatepost/{id}', ['as' => 'user.update_post',    'uses' => 'UserController@updatePost']);
    Route::get('/changepass',       ['as' => 'user.change_pass',    'uses' => 'UserController@changePassword']);
    Route::post('/execchangepass',  ['as' => 'user.exec_change_pass','uses' => 'UserController@execChangePassword']);
    Route::get('/deletepost/{id}',  ['as' => 'user.delete_post',    'uses' => 'UserController@deletePost']);

});

// Chat
Route::group( array('prefix' => 'messages', 'before' => 'auth', 'namespace' => 'messages'), function () {
    Route::get('/',                 ['as' => 'messages.index',      'uses' => 'MessagesController@index']);
    Route::get('/{id}',             ['as' => 'messages.show',       'uses' => 'MessagesController@show']);
    Route::post('/store',           ['as' => 'messages.send_message',      'uses' => 'MessagesController@store']);
});

// Admin group
Route::group(array('prefix' => 'admin', 'before' => 'isAdmin', 'namespace' => 'admin'), function ()
{
    Route::get('/',                 ['as' => 'admin.index',         'uses' => 'DashboardController@index']);
    Route::get('/users',            ['as' => 'admin.users',         'uses' =>  'UsersController@index']);
    Route::get('/users/create',     ['as' => 'admin.users.create',  'uses' =>  'UsersController@create']);
    Route::post('/users/store',     ['as' => 'admin.users.store',   'uses' =>  'UsersController@store']);
    Route::get('/user/show/{id}',   ['as' => 'admin.user.show',     'uses' =>  'UsersController@show']);
    Route::get('/user/edit/{id}',   ['as' => 'admin.user.edit',     'uses' =>  'UsersController@edit']);
    Route::post('/user/update/{id}',['as' => 'admin.user.update',   'uses' =>  'UsersController@update']);
    Route::get('/user/delete/{id}', ['as' => 'admin.user.delete',   'uses' =>  'UsersController@destroy']);
    Route::get('/user/block/{id}',  ['as' => 'admin.user.block',    'uses' =>  'UsersController@block']);
    Route::post('/user/updateavatar/{id}', ['as' => 'admin.user.updateavatar', 'uses' =>  'UsersController@updateAvatar']);
    Route::get('/messages',         ['as' =>'admin.messages',       'uses' => 'MessagesController@index']);
    Route::get('/messages/{id}',    ['as' =>'admin.messages.view',  'uses' => 'MessagesController@show']);
});

// API
Route::group(array('prefix' => 'api'), function ()
{
    Route::group(array('prefix' => 'v1', 'namespace' => 'V1'), function ()
    {
        Route::group(array('prefix' => 'friends'), function()
        {
            Route::get('/get',    ['as' => 'friends.get',        'uses' => 'FriendsController@getFriends']);
        });

        Route::group(array('prefix' => 'messages', 'before' => 'auth.token'), function()
        {
            Route::get('/get',     ['as' => 'messages.get',     'uses' => 'MessagesController@getMessages']);
            Route::post('/store',  ['as' => 'messages.store',   'uses' => 'MessagesController@storeMessages']);
            Route::post('/update', ['as' => 'messages.update',  'uses' => 'MessagesController@updateMessage']);
            Route::post('/delete', ['as' => 'messages.delete',  'uses' => 'MessagesController@deleteMessage']);
        });
    });       
});
