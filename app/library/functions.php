<?php


	/*
	* Check duplicate email
	*/
	function existEmailUpdate($args)
	{
		$exist_email_current_user = \DB::table('users')->where('email', $args['email'])->where('id', $args['id'])->first();
		if(is_null($exist_email_current_user))
		{
			$exist_email_other_user = \DB::table('users')->where('email', $args['email'])->first();
			if(is_null($exist_email_other_user)){
				return false;
			}
			return true;
		}
		else
			return false;
		
	}

	/**
	 * Upload image
	 * @param array $args 
	 * @return string $new_name_image
	 */

	function uploadImage($args)
	{
		$new_name = str_random(16).'_'.date('YmdHis').'.'.$args->getClientOriginalExtension();
		$args->move(public_path('uploads'), $new_name);
		return $new_name;	
	}

	function generateUserId()
	{

		do{
			$new_id = str_random(8).'_'.date('Ymdhis');
		} while( \DB::table('users')->where('id', $new_id) === $new_id );

		return $new_id;
	}


	/**
	* @param string $gender
	* @return int $gender_code
	*
	**/
	function setGender($gender)
	{
		switch ($gender) {
			case 'female':
				return -1;
				break;
			case 'other':
				return 0;
				break;
			default:
				//male
				return 1;
				break;
		}

	}

	function encodePlainText($plaintext = false)
	{
	    if($plaintext != false)
	    {
	    	# create a random IV to use with CBC encoding
		    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

		    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		    
		    # creates a cipher text compatible with AES (Rijndael block size = 128)
		    
		    $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, KEY_ENCRYPTION, $plaintext, MCRYPT_MODE_CBC, $iv);

		    # prepend the IV for it to be available for decryption
		    $ciphertext = $iv . $ciphertext;
		    
		    # encode the resulting cipher text so it can be represented by a string
		    $ciphertext_base64 = base64_encode($ciphertext);

		    return  $ciphertext_base64;	
	    }

	    return $plaintext;
	}

	function decodePlainText($encode_string = false)
	{
		if($encode_string != false)
		{
	    	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

	    	$ciphertext_dec = base64_decode($encode_string);
	    
	    	# retrieves the IV, iv_size should be created using mcrypt_get_iv_size()
	    	$iv_dec = substr($ciphertext_dec, 0, $iv_size);
	    
	    	# retrieves the cipher text (everything except the $iv_size in the front)
	    	$ciphertext_dec = substr($ciphertext_dec, $iv_size);

	    	# may remove 00h valued characters from end of plain text
	    	$plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, KEY_ENCRYPTION, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);
	    	return $plaintext_dec;
		}
		return $encode_string;
		
	}

	function sendEmail( $params = array() )
	{
		if (isset($params['confirm_code']) && isset($params['email']) && $params['subject']) {

			$data = array(
				'token' 	=> $params['confirm_code']
			);

			Mail::send('emails.confirm_register', $data, function ($message) use ($params) {
				$message->to($params['email'])
						->subject($params['subject']);
			});

		} else {

			return false;
		}

		return true;
	}

?>