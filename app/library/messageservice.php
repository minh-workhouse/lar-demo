<?php

	function getMessage($params = array())
	{
		if(isset($params['from_us']) && isset($params['to_us'])){

			$raw_data = \DB::table('messages')
						->where(array('from_us' => $params['from_us'], 'to_us' => $params['to_us']))
						->orWhere(array('from_us' => $params['to_us'], 'to_us' => $params['from_us']))
						->orderBy('created_at', 'ASC')
						->get();
			
			if(count($raw_data) > 0){	

				$data = array();
				foreach ($raw_data as $k => $val) {
					
					$type = $val->from_us == $params['from_us'] ? 'sender' : 'receiver';
					
					$arr = array(
						'id' 		=> $val->id,
						'content' 	=> decodePlainText($val->content),
						'created_at' => $val->created_at,
						'updated_at' => $val->updated_at,
						'type'		=> $type
					);
					array_push($data, $arr);
				}
				
				return $data;

			}
		}

		return false;
	}

	function storeMessage($params = array())
	{
		if(isset($params['from_us']) && isset($params['to_us']) && isset($params['content'])){

			$data_insert = array(
				'from_us' => $params['from_us'],
				'to_us'		=> $params['to_us'],
				'content' => encodePlainText($params['content']),
				'created_at' => date('Y-m-d H:m:i')
			);

			if(\DB::table('messages')->insert( $data_insert ))
				return true;
		}

		return false;
	}


	function getFriends($params = array())
	{
		$data = \DB::table('users')->select('friends')->where('id', $params['user_id'])->first();
		
		if(!is_null($data)){

			$data_return  = array();
			$list_friends_id = json_decode($data->friends);
			$list_friends_id = $list_friends_id->friends;
			foreach ($list_friends_id as $key => $val) {
				$friend_name = \DB::table('users')->select('name')->where('id', $val->id)->first();
				$arr = array('user_id' => $val->id, 'user_name' => $friend_name->name);
				array_push($data_return, $arr);
			}
			
			return $data_return;
		}

		return false;
	}
?>