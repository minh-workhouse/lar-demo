<?php

namespace user;
use Validator;
use Redirect;
use Session;
use Input;
use Auth;
use Hash;
use View;
use DB;

use App\components\PostComponent;
use App\components\UserComponent;
use App\components\UserTempComponent;


class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('users.index');
	}

	public function register()
	{
		return View::make('users.register');
	}

	public function execRegister()
	{	
		$input = Input::all();
		$rules = array(
			'username' 	=> 'required',
			'email' 	=> 'required|unique:users|max:255',
			'password' 	=> 'required',
			'password_confirm' 	=> 'required',
		);

		$validator = Validator::make($input, $rules);

		if ($validator->passes()) {

			$confirm_code = str_random(60);

			$data_insert = array(
				'user_name' 	=> Input::get('username'),
				'email' 		=> Input::get('email'),
				'confirm_code' 	=> $confirm_code,
				'password'		=> Hash::make(Input::get('password')),
				'created_at'	=> date('Y-m-d H:m:s'),
			);

			$params_to_mail = array(
				'confirm_code' 	=> $confirm_code,
				'user_name'		=> Input::get('username'),
				'email'			=> Input::get('email'),
				'subject'		=> 'Email confirm regsiter'
			);

			$users_temp = new UserTempComponent();

			DB::beginTransaction();

			try {

				$users_temp->store($data_insert);
				sendEmail($params_to_mail);
				$mgs = 'Register success, please confirm your email and complete the final registration';

			} catch (Exception $e) {

				DB::rollback();
			}

			DB::commit();
			
			return Redirect::to('user/register')->with('mgs', $mgs);
		}
		else
			return Redirect::to('user/register')->withErrors($validator);

	}

	public function registerConfirm($confirm_code)
	{
		if (isset($confirm_code)) {

			$user_temp = new UserTempComponent();

			$params['confirm_code'] = $confirm_code;

			$user_info = $user_temp->get($params);
			
			if ($user_info != false) {

				$data_insert = array(
					'id' 			=> generateUserId(),
					'user_name' 	=> $user_info->user_name,
					'email'			=> $user_info->email,
					'password' 		=> $user_info->password,
					'created_at' 	=> $user_info->created_at
				);

				$user = new UserComponent();

				if($user->store($data_insert)) {

					$user_temp->delete($user_info->email);
					$mgs = 'Confirm success, You can login';

				} else {
					$mgs = 'Confirm failure';					
				}
			}
		}

		return Redirect::to('user/register')->with('mgs', $mgs);
	}


	public function createPost()
	{
		return View::make('users.create_post');	
	}

	/**
	 * Get all my post
	 *
	 * @return Response
	 */
	public function myPost()
	{
		$params = array(
			'user_id' 	=> Auth::user()->id,
			'order_by'	=> array(
				'column'	=> 'created_at',
				'type'		=> 'DESC'
				),
			'pagination' 	=> 15
		);

		$posts = new PostComponent;
		$list_post = $posts->get($params);

		return View::make('users.mypost', compact('list_post'));
	}

	/**
	 * Store post
	 *
	 * @return Response
	 */
	public function storePost()
	{
		$input = Input::all();
		$rules = array(
			'title_post' 	=> 'required',
			'content_post' => 'required',
		);

		$validator = Validator::make($input, $rules);

		if ($validator->passes()) {

			$params = array(
				'post_title' 	=> Input::get('title_post'),
				'user_id' 		=> Auth::user()->id,
				'post_content' 	=> Input::get('content_post'),
				'created_at' 	=> date('Y-m-d H:m:s'),
			);

			$posts = new PostComponent;

			if ($posts->store($params)) {
				$mgs = 'Create post success';
			} else {
				$mgs = 'Create post failure';
			}
						
			return Redirect::to('user/createpost')->with('mgs', $mgs);

		} else 
			return Redirect::to('user/createpost')->withErrors($validator);
	}

	public function editPost($post_id)
	{
		$params = array('post_id' => $post_id);
		$posts = new PostComponent;

		$post = $posts->get($params);
		
		return View::make('users.edit_post')->with('post', $post[0]);
	}

	public function updatePost()
	{	
		$input = \Input::all();
		$rules = array(
			'title_post' 	=> 'required',
			'content_post' => 'required',
		);

		$validator = Validator::make($input, $rules);

		if($validator->passes()){

			$data_update = array(
				'title' => \Input::get('title_post'),
				'content' => \Input::get('content_post'),
				'updated_at' 	=> date('Y-m-d H:i:s')
			);
			
			\DB::table('post')->where('id', \Input::get('post_id'))->where('user_id', Session::get('user_info')->id)->update( $data_update)  ? $mgs = 'update post success' : $mgs = 'update post failure';
			
			return Redirect::to('user/editpost/'.\Input::get('post_id'))->with('mgs', $mgs);
		}
		else
			return Redirect::to('user/editpost/'.\Input::get('post_id'))->withErrors($validator);
	}

	public function deletePost($post_id)
	{
		if(\Post::find($post_id)->delete()){
			return Redirect::to('user/mypost/')->with('mgs', 'Delete post success');	
		}

		return Redirect::to('user/mypost/')->with('mgs', 'Delete post failure');	

	}


	public function changePassword()
	{
		return View::make('users.change_password');
	}

	public function execChangePassword()
	{
		$input = \Input::all();
		$rules = array(
			'old_password' 	=> 'required',
			'new_password' => 'required|min:6',
			'confirm_password' => 'required|min:6|Matchpass:'.\Input::get('new_password')
		);
		
		$messages = array(
    		'password_confirm.Matchpass' => 'Password confirm not match.',
		);

		$validator = \Validator::make($input, $rules, $messages);

		if($validator->passes()){
			
			if(\Hash::check(\Input::get('old_password'), Session::get('user_info')->password))
			{
				$has_new_password = \Hash::make(\Input::get('new_password'));

				$rs = \DB::table('users')
						->where('id', Session::get('user_info')->id)
						->where('email', Session::get('user_info')->email)
						->update( array('password' => $has_new_password ));
				if($rs == 1){
					
					\Session::flush();					
					return Redirect::to('/')->with('mgs', 'Change password success, please login again');
				}

				else
				  $mgs = 'Change password failure';
				
			}	
			else {
				$mgs = 'Old password wrong';
			}

			return Redirect::to('user/changepass')->with('mgs', $mgs);
		}
		else
			return Redirect::to('user/changepass')->withErrors($validator);	
	}

}
