<?php
namespace messages;

use MessageServices;
use Response;
use Session;
use Auth;
use View;

class MessagesController extends \BaseController
{
	protected $user_id;
	protected $list_friends;

	public function __construct()
	{
		$this->user_id = Auth::user()->id;

		// $params['user_id'] = $this->user_id;
		// $this->list_friends = getFriends($params);
	}

	public function index()
	{

		// /return View::make('messages.index', compact('receiver_info'))->with('list_friends',$this->list_friends);
	}

	public function show($to_us)
	{
		$params = array(
			'from_us' => $this->user_id, 
			'to_us' => $to_us,
		);
		
		$receiver_info = \DB::table('users')->select('id','name')->where('id', $to_us)->first();
		$messages = getMessage($params);

		return View::make('messages.index', compact('messages', 'receiver_info'))->with('list_friends', $this->list_friends);
	}

	public function store()
	{
		$input = \Input::all();
		
		if($input['to_us'] !== '' && $input['message'] !== ''){
			$params = array(
				'from_us' => $this->user_id,
				'to_us' => $input['to_us'],
				'content'	=> $input['message']
			);

			storeMessage($params);

			$redis = \LRedis::connection();
			$redis->set('message', $input['message']);
		}

		return \Redirect::to('messages'.'/'.$input['to_us']);
	}


}
