<?php

use App\components\UserTempComponent;
use App\components\PostComponent;

class HomeController extends BaseController {

	public function index()
	{
		$posts = new PostComponent;

		$params = array('pagination' => '15');
		$list_post = $posts->get($params);
		
		return View::make('post.list', compact('list_post'));
	}

	public function login()
	{
		return View::make('auth.login');	
	}

	public function execLogin()
	{
		$input = Input::all();
		$rules = array(
			'email' 	=> 'required',
			'password' => 'required|min:6'
		);
		
		$validator = Validator::make($input, $rules);

		if($validator->passes()) {

			if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'))))
			{
				if (Auth::user()->level == 0) {
					return Redirect::to('admin');
				}

				return Redirect::to('user');
			}

			return Redirect::to('/login')->with('mgs', 'Login failure, try again');

		} else {
			return Redirect::to('/login')->withErrors($validator);
		}
	}

	public function logout()
	{
		Session::flush();
		return Redirect::to('/login');
	}

}

