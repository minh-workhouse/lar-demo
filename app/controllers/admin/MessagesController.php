<?php
namespace admin;
use View;
class MessagesController extends \BaseController {

	public function index()
	{
		$list_messages = \DB::table('messages')->orderBy('messages.created_at', 'DESC')->paginate(50);
		
		return View::make('admin/messages_users.index', compact('list_messages'));
	}

	public function show($message_id)
	{
		$message = \Messages::find($message_id)->toArray();
		$sender_info = \DB::table('users')->select('id', 'name', 'email')->where('id', $message['from_us'])->first();
		$receiver_info = \DB::table('users')->select('id', 'name', 'email')->where('id', $message['to_us'])->first();
		
		$data_return = array(
			'message_id' => $message['id'],
			'sender_info' => $sender_info,
			'receiver_info' => $receiver_info,
			'content' => decodePlainText($message['content']),
			'created_at'	=> $message['created_at'],
			'updated_at'	=> $message['updated_at']
		);
		
		return View::make('admin/messages_users.show', compact('data_return'));	
	}
}