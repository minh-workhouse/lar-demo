<?php
namespace admin;
use View;
use Validator;
use Redirect;
use Input;
use User;
use UserImage;
use App\components\UserComponent;
use App\components\PostComponent;

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user  = new UserComponent();

		$params = array(
			'order_by'		=> array(
				'column' => 'created_at',
				'type'	 => 'DESC'
			),
			'pagination' 	=> '15'
		);

		$list_users = $user->get($params);
		
		return View::make('admin/users.index', compact('list_users'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin/users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{	
		$input = Input::all();
		$rules = array(
			'email' 		=> 'required|unique:users|max:255',
			'user_name' 	=> 'required',
			'avatar' 		=> 'required',
			'date_of_birth' => 'required|before:now',
			'gender'		=> 'required|in:male,female,other',
			'phone' 		=> 'numeric'
		);

		$validator = Validator::make($input, $rules);

		if ($validator->passes()) {

			$params = array(
				'user_name' 	=> Input::get('user_name'),
				'email'			=> Input::get('email'),
				'password'		=> '',
				'avatar' 		=> Input::file('avatar'),
				'date_of_birth' => Input::get('data_of_birth'),
				'gender'		=> Input::get('gender'),
				'phone'			=> Input::get('phone'),
				'address'		=> Input::get('address')
			);

			$user   = new UserComponent();
			$result = $user->store($params);

		}
		else
			return Redirect::to('admin/users/create')->withErrors($validator);
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// $user = \DB::table('users')
		// 			->join('user_images', 'users.id', '=', 'user_images.user_id' )
		// 			->select('users.id', 'users.name', 'users.date_of_birth', 'users.gender', 'users.address', 'users.email', 'users.phone', 'users.created_at', 'users.updated_at', 'user_images.url')
		// 			->where('users.id', $id)->first();
		// var_dump($user);
		// $list_post = \DB::table('post')
		// 				->select('id', 'title','created_at', 'updated_at')
		// 				->where('user_id', $id)
		// 				->get();
		
		// if(is_null($user))
		// 	

		// return View::make('admin/users.show', compact('user', 'list_post'));
		$us = new UserImage;
		var_dump($us->getUrlImage($id));
		// $params = array(
		// 	'user_id' => $id
		// );
		// $user = new UserComponent;
		// $user_data = $user->get($params);
			
		// if (count($user_data) > 0) {
				
		// } else {
		// 	return Redirect::to('admin/users');
		// }
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$user = \DB::table('users')
					->join('user_images', 'users.id', '=', 'user_images.user_id' )
					->select('users.id', 'users.name', 'users.date_of_birth', 'users.gender', 'users.address', 'users.email', 'users.phone', 'users.created_at', 'users.updated_at', 'user_images.url')
					->where('users.id', $id)->first();

		if(is_null($user))
			return Redirect::to('admin/users');

		return View::make('admin/users.edit', compact('user'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(existEmailUpdate(array('id' => $id, 'email' => \Input::get('email')))){
			return Redirect::to('admin/user/edit/'.$id)->with('mgs', 'Duplicate Email');
		}

		$input = \Input::all();
		$rules = array(
			'email' 	=> 'required',
			'user_name' => 'required',
			'date_of_birth' => 'required|before:now',
			'gender'	=> 'required|in:male,female,other',
			'phone' 	=> 'numeric'
		);

		$validator = Validator::make($input, $rules);
		
		if($validator->passes()){

			$data_update = array(
				'name' => \Input::get('user_name'),
				'date_of_birth' => \Input::get('date_of_birth'),
				'address'		=> \Input::get('address'),
				'phone'			=> \Input::get('phone'),
				'gender'		=> setGender(\Input::get('gender')),
				'created_at'	=> \Input::get('created_at'),
				'updated_at' 	=> date('Y-m-d H:i:s')
			);
			
			if(\DB::table('users')->where('id', $id)->update( $data_update ))
			{
				$mgs = 'update success';
			}
			else
				$mgs = 'update failure';
			return Redirect::to('admin/user/edit/'.$id)->with('mgs', $mgs);
		}
		else
			return Redirect::to('admin/user/edit/'.$id)->withErrors($validator);
	}

	/**
	 * Block user
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function block($id)
	{
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$get_name_file_avatar = \DB::table('users')
					->join('user_images', 'users.id', '=', 'user_images.user_id' )
					->select('user_images.url')
					->where('users.id', $id)->first();
		
		if(!is_null($get_name_file_avatar)) {
			\File::delete((public_path('uploads').'/'.$get_name_file_avatar->url));
		}

		\User::find($id)->delete();
		
		return Redirect::to('admin/users')->with('mgs', 'Delete success');

	}

	/**
	 * Update avatar
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function updateAvatar($id)
	{

	}
}
