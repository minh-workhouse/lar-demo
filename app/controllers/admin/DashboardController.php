<?php
namespace admin;
use View;
class DashboardController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin/dashboard.index');
	}

}
