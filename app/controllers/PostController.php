<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		echo "create post";
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = \DB::table('post')
					->join('users', 'users.id', '=', 'post.user_id')
					->select('post.id', 'post.title', 'post.content', 'post.updated_at', 'post.created_at', 'users.name as author_name')
					->where('post.id', $id)
					->first();
		if(is_null($post))
			return Redirect::to('/user');

		return \View::make('post.show')->with('post', $post);
	}


	


}
