<?php
namespace V1;
use Response;
use Input;

class MessagesController extends \BaseController {

	public function getMessages()
	{
		
		$params = Input::all();
		
		if (!isset($params['from_us']) && !isset($params['to_us'])) {
			$data_return = array(
				'result' => false,
				'data'	=> ''
			);
			$code = 400;
		} else {
			$rs = getMessage($params);
			
			if( $rs == false )
			{
				$data_return = array(
					'result' => false,
					'data'	=> ''
				);
			}
			else
			{
				$data_return = array(
					'result' => true,
					'data'	=> $rs
				);
			}
			$code = 200;
			
		}

		return Response::json($data_return, $code);

	}

	public function storeMessages()
	{
		$params = Input::all();
		if(!isset($params['from_us']) && !isset($params['to_us']) && !isset($params['content']))
		{
			$data_return = array(
				'result' => false,
				'data' => ''
			);
			$code = 400;
		}
		else
		{
			if(storeMessage($params))
			{
				$data_return = array(
					'result' => true
				);
			}
			else
			{
				$data_return = array(
					'result' => false
				);
			}
			
			$code = 200;
		}

		return Response::json($data_return, $code);
	}

	public function updateMessage()
	{
		$params = Input::all();
		if(!isset($params['mgs_id']) && !isset($params['from_us']) && !isset($params['to_us']) && !isset($params['content']))
		{
			$data_return = array(
				'result' => false,
				'data' => ''
			);
			$code = 400;
		}
		else
		{
			$data_update = array(
				'content' => encodePlainText($params['content']),
				'updated_at' => date('Y-m-d H:m:s')
			);

			$rs = \DB::table('messages')->where('id', $params['mgs_id'])
										->where('from_us', $params['from_us'])
										->where('to_us', $params['to_us'])
										->update( $data_update );
			if($rs)
			{
				$data_return = array('result' 	=> true);
			}
			else
			{
				$data_return = array('result' 	=> false);	
			}

			$code = 200;
		}

		return Response::json($data_return, $code);
	}

	public function deleteMessage()
	{
		$params = Input::all();
		if(!isset($params['mgs_id']) && !isset($params['from_us']) && !isset($params['to_us']))
		{
			$data_return = array(
				'result' => false,
				'data' => ''
			);
			$code = 400;
		}
		else
		{
			$rs = \DB::table('messages')->where('id', $params['mgs_id'])
										->where('from_us', $params['from_us'])
										->where('to_us', $params['to_us'])
										->delete();
			if($rs)
			{
				$data_return = array('result' 	=> true);
			}
			else
			{
				$data_return = array('result' 	=> false);	
			}

			$code = 200;
		}

		return Response::json($data_return, $code);
	}
}
