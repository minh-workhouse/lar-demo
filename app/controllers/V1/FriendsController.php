<?php
namespace V1;
use Response;

class FriendsController extends \BaseController {

	public function getFriends()
	{
		$params = \Input::all();

		if(!isset($params['user_id']))
		{
			$data_return = array(
				'result' => false,
				'data' => ''
			);
			$code = 400;
		}
		else
		{
			$data = \DB::table('users')->select('friends')->where('id', $params['user_id'])->first();
			
			if(count($data) > 0){	
				$data_return = array(
					'result' 	=> true,
					'data' 		=> json_decode($data->friends),
				);

				$code = 200;
			}
			else
			{
				$data_return = array(
					'result' 	=> false,
					'data' 		=> '',
				);

				$code = 204;
			}
		}
		return Response::json($data_return, $code);
	}
}
