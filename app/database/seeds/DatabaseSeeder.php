<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
        DB::table('users')->truncate();
        $this->call('UserTableSeeder');
	}

}
class UserTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 1000; $i++) {
            DB::table('Users')->create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $faker->password,
                'create_at' => $faker->dateTime(),
            ]);
        }

    }

}
