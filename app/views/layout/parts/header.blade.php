<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ URL::route('home.index')}}">Demo Site</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     <ul class="nav navbar-nav navbar-right">
      @if (isset(Auth::user()->level) && Auth::user()->level == 0)
        <li><a href="{{ URL::route('admin.index') }}">Admin Cpanel</a></li>
      @endif

      @if (Auth::check())
        <li><a href="{{ URL::route('messages.index') }}">My chat</a></li>
        <li><a href="{{ URL::route('user.mypost') }}">My posts</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Setting <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Update profile</a></li>
            <li><a href="{{ URL::route('user.change_pass') }}">Change password</a></li>
            <li><a href="{{ URL::route('home.logout') }}">Logout</a></li>    
          </ul>
        </li>
      @else
        <li><a href="{{ URL::route('user.register') }}">Register</a></li>
        <li><a href="{{ URL::route('home.login') }}">Login</a></li>
      @endif
      
     </ul>
      <!-- <ul class="nav navbar-nav navbar-right">
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
