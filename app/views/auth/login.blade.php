
@extends('layout/master')
@section('body')
<div class="container">
	<div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">	
		@if (Session::has('mgs'))
		<div class="alert alert-info">{{ Session::get('mgs') }}</div>
        @endif
        {{ Form::open(array('route' => 'home.execLogin', 'role'=>'form')) }}
        <div class="user-login">
            <h3 class="text-center default-text py-3"><i class="fa fa-lock"></i>Login</h3>
            <!--Body-->
            <div class="md-form">
                <label for="defaultForm-email">Your email</label>
                <input type="email" if="defaultForm-email" name="email" class="form-control">
                <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('email'); ?></small>
            </div>
            <div class="md-form">
                <label for="defaultForm-pass">Your password</label>
                <input type="password" id="defaultForm-pass" name="password" class="form-control">
                <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('password'); ?></small>
            </div>
            <div class="text-center">
                <button class="btn btn-primary">Login</button>
            </div>
        </div>
        {{ Form::close() }}
		
	</div>
</div>


@stop