@extends('layout/master')
@section('body')
<div class="container">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-md-offset-3 col-lg-offset-3">
      @if (Session::has('mgs'))
            <div class="alert alert-info">{{ Session::get('mgs') }}</div>
      @endif

      {{ Form::open(array('route' => 'user.exec_register', 'role'=>'form')) }}
        <fieldset>
          <div class="form-group"><legend>Register</legend></div>
          <div class="form-group">
            <!-- Username -->
            <label class="control-label"  for="username">Username</label>
            <div class="controls">
              <input type="text" id="username" name="username" class="form-control">
              <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('username'); ?></small>
            </div>
          </div>
          <div class="form-group">
            <!-- E-mail -->
            <label class="control-label" for="email">E-mail</label>
            <div class="controls">
              <input type="text" id="email" name="email" class="form-control">
              <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('email'); ?></small>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label" for="password">Password</label>
            <div class="controls">
              <input type="password" id="password" name="password" class="form-control">
              <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('password'); ?></small>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label" for="password_confirm">Confirm password</label>
            <div class="controls">
              <input type="password" id="password_confirm" name="password_confirm" class="form-control">
              <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('password_confirm'); ?></small>
            </div>
          </div>
          <div class="form-group">
             <div class="controls">
              <button type="submit" class="btn btn-success">Register</button>
            </div> 
          </div>
        </fieldset>
      {{ Form::close() }}  
    </div>
  </div>
</div>

@stop