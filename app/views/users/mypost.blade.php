
@extends('layout/master')
@section('body')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="btn-group">
                  <a href="{{ URL::route('user.create_post') }}" class="btn btn-primary">New post</a>
                </div>      
            </div>
        </div>

        @if (Session::has('mgs'))
        <hr>
            <div class="alert alert-info">{{ Session::get('mgs') }}</div>
        @endif
        <hr>
        <?php if(count($list_post->toArray()['data']) <= 0 ) echo "No have post"; else { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <table class="table">
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Date Created</th>
                            <th>Date update</th>
                            <th>Action</th>
                        </tr>
                    <?php 
                    
                    foreach ($list_post as $post) { ?>
                        <tr>
                            <td>{{ $post->post_id }}</td>
                            <td><a href="{{ URL::to('post').'/'.$post->post_id }}">{{ $post->title }}</a></td>
                            <td>{{ $post->created_at }}</td>
                            <td>{{ $post->updated_at }}</td>
                            <td><a href="{{ URL::to('user/editpost').'/'.$post->post_id }}" data-original-title="Edit this post" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a> <a data-original-title="Block this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" href="{{ URL::to('user/deletepost').'/'.$post->post_id }}"><i class="glyphicon glyphicon-remove"></i></td>
                        </tr>
                    <?php }
                    ?>
                    </table>
                    
                </div>
                <?php echo $list_post->links(); ?>     
            </div>
        </div>
        <?php } ?>
    </div>

@stop