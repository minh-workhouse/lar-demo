@extends('layout/master')
@section('body')
<div class="container">
	<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3">
		@if (Session::has('mgs'))
		   <div class="alert alert-info">{{ Session::get('mgs') }}</div>
		@endif
		{{ Form::open(array('route' => 'user.exec_change_pass', 'role'=>'form')) }}
			<legend>Change password</legend>
			<div class="form-group">
				<label for="oldpass">Old password</label>
				<input type="password" class="form-control" id="oldpass" name="old_password">
				<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('old_password'); ?></small>
			</div>
			<div class="form-group">
				<label for="new_password">New password</label>
				<input type="password" class="form-control" id="new_password" name="new_password">
				<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('new_password'); ?></small>
			</div>
			<div class="form-group">
				<label for="confirm_password">Confirm new password</label>
				<input type="password" class="form-control" id="confirm_password" name="confirm_password">
				<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('confirm_password'); ?></small>
			</div>
			<button type="submit" class="btn btn-primary">Change</button>
		{{ Form::close() }}
	</div>
</div>
@stop