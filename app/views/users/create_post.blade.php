@extends('layout/master')
@section('body')
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				@if (Session::has('mgs'))
				   <div class="alert alert-info">{{ Session::get('mgs') }}</div>
				@endif
				<div class="panel panel-default">
					<div class="panel-heading">Add new post</div>
						<div class="panel-body">
					{{ Form::open(array('route' => 'user.store_post', 'role'=>'form')) }}
						<div class="form-group">
							<label for="title-post">Title</label>
							<input type="text" name="title_post" class="form-control" id="title-post">
							<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('title_post'); ?></small>
						</div>
						<div class="form-group">
							<label>Content</label>
							<textarea cols="30" name="content_post" rows="10" class="form-control"></textarea>
							<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('content_post'); ?></small>
						</div>
						
						<button type="submit" class="btn btn-primary">Add new</button>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
	</div>
	
    
@stop