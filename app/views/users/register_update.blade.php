@extends('layout/master')
@section('body')
<div class="container">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-md-offset-3 col-lg-offset-3">
      @if (Session::has('mgs'))
            <div class="alert alert-info">{{ Session::get('mgs') }}</div>
      @endif
      {{ Form::open(array('route' => 'user.register_confirm', 'role'=>'form', 'files' => true)) }}
        <fieldset>
          <div class="form-group"><legend>Update infomation</legend></div>
          <div class="form-group">
            <label class="control-label">Username</label>
            <div class="controls">
              <input type="text" name="username" class="form-control" value="{{ $user_info->user_name }}">
              <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('username'); ?></small>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label" for="email">E-mail</label>
            <div class="controls">
              <div class="form-control">{{ $user_info->email }}</div>
            </div>
          </div>
          <div class="form-group">
            <label>Date of birth</label>
            <input name="date_of_birth" type="date" class="form-control">
            <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('date_of_birth'); ?></small>
          </div>
          <div class="form-group">
            <label>Gender</label>
            <select name="gender" class="form-control">
              <option value="1">Male</option>
              <option value="-1">Female</option>
              <option value="0">Other</option>
            </select>
            <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('gender'); ?></small>
          </div>
          <div class="form-group">
            <label class="control-label">Address</label>
            <div class="controls">
              <input type="text" name="address" class="form-control">
              <small class="form-text"><?php if(isset($errors)) echo $errors->first('address'); ?></small>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label">Phone</label>
            <div class="controls">
              <input type="text" name="phone" class="form-control">
              <small class="form-text"><?php if(isset($errors)) echo $errors->first('phone'); ?></small>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label">Password</label>
            <div class="controls">
              <input type="password" name="password" class="form-control">
              <small class="form-text"><?php if(isset($errors)) echo $errors->first('password'); ?></small>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label">Password Confirm</label>
            <div class="controls">
              <input type="password" name="password_confirm" class="form-control">
              <small class="form-text"><?php if(isset($errors)) echo $errors->first('password_confirm'); ?></small>
            </div>
          </div>
          <div class="form-group">
            <label for="InputFile">Upload avatar</label>
            <input type="file" name="avatar" class="form-control-file" id="InputFile" aria-describedby="fileHelp">
            <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('avatar'); ?></small>
          </div>
          <div class="form-group">
             <div class="controls">
              <button type="submit" class="btn btn-success">Update</button>
            </div> 
          </div>
        </fieldset>
      {{ Form::close() }}  
    </div>
  </div>
</div>

@stop