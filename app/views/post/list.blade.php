@extends('layout.master')
@section('body')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
			@if (Session::has('mgs'))
	            <div class="alert alert-info">{{ Session::get('mgs') }}</div>
	      	@endif
			<?php if( count($list_post) > 0 ){
				foreach ($list_post as $post){ ?>
				
				<div class="well">
					<div class="media">
						<div class="media-body">
							<h4 class="media-heading"><a class="text-right" href="{{ URL::to('post').'/'.$post->post_id }}">{{ $post->title }}</a></h4>
							<p class="text-right">By: <strong>{{ $post->author }}</strong></p>
							<div class="post-content">
							{{ substr($post->content, 0, 500).'...' }}
							<a class="text-right" href="{{ URL::to('post').'/'.$post->post_id }}">Read more</a>
							</div>
							
							<ul class="list-inline list-unstyled">
	  							<li><span><i class="glyphicon glyphicon-calendar"></i> Post day: <strong>{{ $post->created_at }}</strong></span></li>
	  						</ul>
						</div>
					</div>
				</div>

			<?php 
				}
				echo $list_post->links();
			} 
			else {
			 	echo "No have post";
			}
			?>

			</div>
		</div>
	</div>
@stop