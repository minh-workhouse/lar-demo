@extends('layout.master')
@section('body')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
			  	<div class="post">
					<h2 class="post-heading">{{ $post->title }}</h2>
					<h5>
					<?php 
					if(is_null($post->updated_at)) {
						echo "Post date: ".$post->created_at;
					}
					else
						echo "Update date: ".$post->updated_at;
					?></h5>
					<div class="post-content">
					{{ $post->content }}
					</div>
					<p class="text-right">By: <strong>{{ $post->author_name }}</strong></p>
			    </div>
			    <div class="wrap-btn">
			    	<a class="btn btn-default" href="{{ URL::route('home.index') }}">Back</a>	
			    </div>
			    
			</div>
		</div>
	</div>
@stop