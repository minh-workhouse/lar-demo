@extends('admin/layout.master')

@section('body')
	@if (Session::has('mgs'))
        <div class="alert alert-info">{{ Session::get('mgs') }}</div>
    @endif
	<hr>
	<?php if( count($list_messages) > 0 ){ ?>

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
			  	<table class="table">
			  		<tr>
			  			<th>Messages Id</th>
			  			<th>From User</th>
			  			<th>To User</th>
			  			<th>Date created</th>
			  			<th>Date updated</th>
			  			<th>Action</th>
			  		</tr>
				<?php 
			  	
			  	foreach ($list_messages as $k => $message) { ?>
			  		<tr>
			  			<td>{{ $message->id }}</td>
			  			<td>{{ $message->from_us }}</td>
			  			<td>{{ $message->to_us }}</td>
			  			<td>{{ $message->created_at }}</td>
			  			<td>{{ $message->updated_at }}</td>
			  			<td><a href="{{ URL::to('admin/messages').'/'.$message->id }}" data-original-title="View message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-eye-open"></i></a> <a data-original-title="Delete message" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" href=""><i class="glyphicon glyphicon-remove"></i></td>
			  		</tr>
			  	<?php }
			  	?>
			  	</table>
			  	
			</div>
			<?php echo $list_messages->links(); ?>		
		</div>
	</div>

	<?php } else { echo "No have messages"; }?>
	
@stop