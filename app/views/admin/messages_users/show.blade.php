@extends('admin/layout.master')

@section('body')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Message id: #{{ $data_return['message_id']}}</div>
				<div class="panel-body">
					<div class="row">
						<div class="col lg-6 col-md-6">
							<div class="sender-info">
								<h4>Sender informations</h4>
								<p><b>Name:</b> {{ $data_return['sender_info']->name }}</p>
								<p><b>Email:</b> {{ $data_return['sender_info']->email }}</p>
							</div>
						</div>
						<div class="col lg-6 col-md-6">
							<div class="sender-info">
								<h4>Receiver informations</h4>
								<p><b>Name:</b> {{ $data_return['receiver_info']->name }}</p>
								<p><b>Email:</b> {{ $data_return['receiver_info']->email }}</p>
							</div>		
						</div>
					</div>
					
					<p><b>Date create:</b> {{ $data_return['created_at'] }}</p>
					<p><b>Date update:</b> {{ $data_return['updated_at'] }} </p>
					<p><b>Content:</b></p>
					<div>{{ $data_return['content'] }}</div>
				</div>
			</div>
			<div class="btn-groups">
				<a class="btn btn-default" href="{{ URL::route('admin.messages') }}">Back</a>	
				<a class="btn btn-danger" href="{{ URL::route('admin.messages') }}">Delete</a>	
			</div>
			

		</div>
	</div>

@stop