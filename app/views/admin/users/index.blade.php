@extends('admin/layout.master')

@section('body')

	<div class="row">
		<div class="col-lg-12">
			<div class="btn-group">
			  <a href="{{ URL::route('admin.users.create') }}" class="btn btn-primary">Add new</a>
			</div>		
		</div>
	</div>

	@if (Session::has('mgs'))
	<hr>
        <div class="alert alert-info">{{ Session::get('mgs') }}</div>
    @endif
	<hr>
	<?php if( count($list_users) > 0 ){ ?>

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
			  	<table class="table">
			  		<tr>
			  			<th>#</th>
			  			<th>Name</th>
			  			<th>Email</th>
			  			<th>Date of birth</th>
			  			<th>Address</th>
			  			<th>Date Created</th>
			  			<th>Gender</th>
			  			<th>Status</th>
			  			<th>Action</th>
			  		</tr>
				<?php 
			  	
			  	foreach ($list_users as $k => $user) { ?>
			  		<tr>
			  			<td>{{ $k + 1 }}</td>
			  			<td><a href="{{ URL::to('admin/user/show/').'/'.$user->id }}" target="_blank">{{ $user->name }}</a></td>
			  			<td>{{ $user->email }}</td>
			  			<td>{{ $user->date_of_birth }}</td>
			  			<td>{{ $user->address }}</td>
			  			<td>{{ $user->created_at }}</td>
			  			<td>
			  			<?php 
                        switch ($user->gender) {
                          case '1':
                            echo 'Male';
                            break;
                          case '-1':
                            echo 'Female';
                            break;
                            
                          default:
                            echo 'Other';
                            break;
                        } 
                        ?> 
			  			</td>
			  			<td>
			  			<?php 
			  			echo $user->status = 1 ? 'Active' : 'Deacitve';

			  			?>	
			  			</td>
			  			<td><a href="{{ URL::to('admin/user/edit/').'/'.$user->id }}" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a> <a data-original-title="Block this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" href="{{ URL::to('admin/user/delete').'/'.$user->id }}"><i class="glyphicon glyphicon-remove"></i></td>
			  		</tr>
			  	<?php }
			  	?>
			  	</table>
			  	
			</div>
			<?php echo $list_users->links(); ?>		
		</div>
	</div>

	<?php } else { echo "No have user"; }?>
	
@stop