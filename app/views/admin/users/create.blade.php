@extends('admin/layout.master')

@section('body')
	@if (Session::has('mgs'))
	   <div class="alert alert-info">{{ Session::get('mgs') }}</div>
	@endif
	
    <div class="panel panel-default">
		<div class="panel-heading">Insert User</div>
		<div class="panel-body">
			{{ Form::open(array('route' => 'admin.users.store', 'role'=>'form', 'files' => true)) }}
				<div class="form-group">
					<label for="exampleInputEmail1">Email address</label>
					<input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
					<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('email'); ?></small>
				</div>
				<div class="form-group">
					<label>Name</label>
					<input type="text" name="user_name" class="form-control">
					<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('user_name'); ?></small>
				</div>
				<div class="form-group">
					<label>Date of birth</label>
					<input name="date_of_birth" type="date" class="form-control">
					<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('date_of_birth'); ?></small>
				</div>
				<div class="form-group">
					<label>Gender</label>
					<div class="form-check">
						<label class="form-check-label">
							<input type="radio" class="form-check-input" name="gender" id="radio-gender" value="male" checked> Male
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<input type="radio" class="form-check-input" name="gender" id="radio-gender" value="female"> Female
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<input type="radio" class="form-check-input" name="gender" id="radio-gender" value="other"> Other
						</label>
					</div>
					<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('gender'); ?></small>
				</div>
				<div class="form-group">
					<label>Address</label>
					<input type="text" name="address" class="form-control">
					<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('address'); ?></small>
				</div>
				<div class="form-group">
					<label>Phone</label>
					<input type="text" name="phone" class="form-control">
					<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('phone'); ?></small>
				</div>
				<div class="form-group">
					<label for="exampleInputFile">Upload avatar</label>
					<input type="file" name="avatar" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
					<small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('avatar'); ?></small>
				</div>
				<button type="submit" class="btn btn-primary">Add new</button>
			{{ Form::close() }}
		</div>
	</div>
@stop