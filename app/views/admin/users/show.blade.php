@extends('admin/layout.master')

@section('body')
  <div class="row">
    
    <div class="col-md-6 col-lg-6">
      <div class="panel panel-info">
        <div class="panel-heading"><h3 class="panel-title">{{ $user->name }}</h3></div>
        <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="No image show" src="{{ URL::to('/uploads/').'/'.$user->url }}" class="img-circle img-responsive"> </div>

                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Date join</td>
                        <td>{{ $user->created_at }}</td>
                      </tr>
                      <tr>
                        <td>Date update</td>
                        <td>{{ $user->updated_at }}</td>
                      </tr>
                      <tr>
                        <td>Date of Birth</td>
                        <td>{{ $user->date_of_birth }}</td>
                      </tr>
                        <tr>
                             <tr>
                        <td>Gender</td>
                        <td>
                        <?php 
                        switch ($user->gender) {
                          case '1':
                            echo 'Male';
                            break;
                          case '-1':
                            echo 'Female';
                            break;
                          default:
                            echo 'Other';
                            break;
                        } 
                        ?>  
                        </td>
                      </tr>
                        <tr>
                        <td>Address</td>
                        <td>{{ $user->address }}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td>{{ $user->email }}</td>
                      </tr>
                        <td>Phone Number</td>
                        <td>{{ $user->phone }}</td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                </div>
              </div>
        </div>
        <div class="panel-footer">
          <a href="{{ URL::to('admin/user/edit/').'/'.$user->id }}" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-6">
      <div class="panel panel-primary">
        <div class="panel-heading">Post list</div>
        <div class="panel-body">
          <?php 
          if(count($list_post) > 0){  ?>
          <table class="table">
              <tr>
                <th>#</th>
                <th>Post title</th>
                <th>Date create</th>
                <th>Last update</th>
              </tr>
            <?php foreach ($list_post as $post) { ?>
            
              <tr>
                <td>{{ $post->id }}</td>
                <td><a href="{{ URL::to('post').'/'.$post->id }}"><?php echo substr($post->title, 0 , 100); ?></a></td>
                <td>{{ $post->created_at }}</td>
                <td>{{ $post->updated_at }}</td>
              </tr>
            
            <?php } ?>
          </table>
          <?php 
          }
          else{
            echo "<h4>No have post</h4>";
          } 
          ?>
        </div>
      </div>
    </div>
  </div>
	
	
@stop