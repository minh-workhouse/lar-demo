@extends('admin/layout.master')

@section('body')
  <div class="row">
    <div class="col-md-8 col-lg-8">
      @if (Session::has('mgs'))
         <div class="alert alert-info">{{ Session::get('mgs') }}</div>
      @endif
      <div class="panel panel-info">
        <div class="panel-heading"><h3 class="panel-title">{{ $user->name }}</h3></div>
        <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center">
                  <img id="img-show" alt="No image show" src="{{ URL::to('/uploads/').'/'.$user->url }}" class="img-circle img-responsive">
                {{ Form::open(array('url' => 'user/updateavatar/'.$user->id)) }}
                  <label class="link-change" for="changeAvatar">Change</label>
                  <input onchange="showImgBeforeUpload(this);" style="display:none" type="file" name="avatar" class="form-control-file" id="changeAvatar" aria-describedby="fileHelp">
                {{ Form::close() }}
                </div>
                <div class=" col-md-9 col-lg-9 "> 
                {{ Form::open(array('url' => 'admin/user/update/'.$user->id)) }}
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>ID</td>
                        <td>{{ $user->id }}</td>
                      </tr>
                      <tr>
                        <td>User name:</td>
                        <td><input name="user_name" type="text" class="form-control" value="{{ $user->name }}">
                        <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('user_name'); ?></small></td>
                      </tr>
                      <tr>
                        <td>Date join:</td>
                        <td><input name="created_at" type="datetime" class="form-control" value="{{ $user->created_at }}">
                        <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('created_at'); ?></small></td>
                      </tr>
                      <tr>
                        <td>Date of birth:</td>
                        <td>
                          <input name="date_of_birth" type="date" class="form-control" value="{{ $user->date_of_birth }}">
                          <small class="form-text text-muted"><?php if(isset($errors)) echo $errors->first('date_of_birth'); ?></small></td>
                        </td>
                      </tr>
                        <tr>
                             <tr>
                        <td>Gender</td>
                        <td>
                          <select name="gender" class="form-control">
                            <option value="male" <?php if($user->gender == 1) { echo "selected= 'selected'"; } ?>>Male</option>
                            <option value="female" <?php if($user->gender == -1) { echo "selected= 'selected'"; } ?>>Female</option>
                            <option value="other" <?php if($user->gender == 0) { echo "selected= 'selected'"; } ?>>Other</option>
                          </select>
                        </td>
                      </tr>
                        <tr>
                        <td>Address</td>
                        <td>
                          <input type="text" name="address" value="{{ $user->address }}" class="form-control">
                        </td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><input type="email" name="email" value="{{ $user->email }}" class="form-control"></td>
                      </tr>
                        <td>Phone Number</td>
                        <td><input type="text" name="phone" value="{{ $user->phone }}" class="form-control"></td>
                      </tr>
                    </tbody>
                  </table>
                  
                </div>
              </div>
        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
        {{ Form::close() }}
      </div>
    </div>
    
  </div>
  
  
@stop