<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Admin Cpanel</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		{{ HTML::style('backend/css/styles.css') }}
		{{ HTML::script('backend/js/scripts.js') }}
	</head>
	<body>

		@include('admin/parts/header')
		<div class="container-fluid">
			<div class="col-lg-2">
				<ul class="nav flex-column aside-nav">
				  <li class="nav-item">
				    <a class="nav-link <?php if(is_null(Request::segment(2))){  echo "active"; } ?>" href="{{ URL::route('admin.index') }}">Dashboard</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link <?php if(Request::segment(2) === 'users'){  echo "active"; } ?>" href="{{ URL::route('admin.users') }}">Users</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link <?php if(Request::segment(2) === 'messages'){  echo "active"; } ?>" href="{{ URL::route('admin.messages') }}">Messages</a>
				  </li>
				</ul>
			</div>
			<div class="col-lg-10">
				
				@yield('body')
				
			</div>
		</div>
		

		@include('layout/parts.footer')
	</body>
</html>