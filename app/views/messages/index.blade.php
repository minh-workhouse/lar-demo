@extends('layout.master')
@section('body')
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="row">
					<div class="col-lg-3 col-md-3">

						<div class="wrap-list-users">
							<div class="list-group">
								<h3 class="title-group">List users</h3>
								@include('layout/parts.message_listfriends')
							</div>
							
						</div>
					</div>	
					<div class="col-lg-9 col-md-9">
						@if(isset($receiver_info))
						<div class="wrap-messages">
							<h3 class="title">Message <span class="user-chat">{{ '#'.$receiver_info->name }}</span></h3>
							<div class="content-messages">
								<div class="show-message">
								@if(isset($messages) && $messages != false )
									@foreach ($messages as $message)
										<div class="<?php echo $message['type'] == 'sender' ? 'send' : 'receiver' ?>">
											<p class="<?php echo $message['type'] == 'sender' ? 'send-mgs' : 'receiver-mgs' ?>">{{ $message['content'] }}</p>
											<small class="time-mgs">{{ $message['created_at'] }}</small>
										</div>
									@endforeach
								@endif
								<div id="messages"></div>
								</div>
								<div class="form-input-message">
									<div class="input-group">
										{{ Form::open(array('route' => 'messages.send_message', 'role'=>'form')) }}
										<input type="hidden" name="to_us" value="{{ $receiver_info->id }}">
										<input type="text" name="message" class="form-control" placeholder="Enter the message">
										<span class="input-group-btn"><button class="btn btn-default" type="submit">Send</button></span>
										{{ Form::close() }}
									</div>
							    </div>
							</div>
						</div>
						@endif
					</div>
				</div>
			</div>	
		</div>
	</div>
	
@stop