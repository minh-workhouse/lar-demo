<?php

namespace App\components;
use DB;
use Hash;

class UserComponent {

	protected $tb;

	public function __construct()
	{
	  $this->tb = DB::table('users');
	}

	/**
	 * 
	 *
	 * @param  array  $params
	 * @return data
	 */

    public function get($params = array())
  	{	
		$this->tb->where('level', '>', '0');

		if (isset($params['user_id'])) {
			$this->tb->where('users.id', $params['user_id']); 
		}

		if (isset($params['order_by'])) {
			$this->tb->orderBy($params['order_by']['column'], $params['order_by']['type']); 
		}

		if (isset($params['pagination'])) {

		$data = $this->tb->paginate($params['pagination']);  	

		} else {

		$data = $this->tb->get();
		}

		return $data;
  	}

  	/**
	 * Insert user, avatar and send email attach link active account
	 *
	 * @param  array  $params
	 * @return boolean 
	 */

  	public function store($params = array())
  	{
	  
	  $data_insert = array(
		'id' 			=> $params['id'],
		'name' 			=> $params['user_name'],
		'email' 		=> $params['email'],
		'password' 		=> $params['password'],
		'created_at' 	=> $params['created_at']
	  );

	  return $this->tb->insert( $data_insert );
	  
  	}

}