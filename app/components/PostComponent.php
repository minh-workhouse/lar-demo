<?php

namespace App\components;
use DB;

class PostComponent {

	protected $tb;

	public function __construct()
	{
	  $this->tb = DB::table('post');
	}

	public function get($params = array())
	{

		$this->tb->join('users', 'users.id', '=', 'post.user_id');
		$this->tb->select('users.name as author', 'post.id as post_id', 'post.title', 'post.content', 'post.created_at', 'post.updated_at');

		if (isset($params['post_id'])) {
			$this->tb->where('post.id', $params['post_id']); 
		}

		if (isset($params['user_id'])) {
			$this->tb->where('post.user_id', $params['user_id']); 
		}

		if (isset($params['order_by'])) {
			$this->tb->orderBy($params['order_by']['column'], $params['order_by']['type']); 
		}

		if (isset($params['pagination'])) {

			$data = $this->tb->paginate($params['pagination']);  	

		} else {

			$data = $this->tb->get();
		}

		return $data;
	}

	public function store($params = array())
	{
		$data_insert = array(
			'title' => $params['post_title'],
			'user_id' => $params['user_id'],
			'content' => $params['post_content'],
			'created_at' => $params['created_at'],
		);
			
		return $this->tb->insert($data_insert);
	}

}