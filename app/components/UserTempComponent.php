<?php

namespace App\components;
use DB;
use Hash;

class UserTempComponent {

	protected $tb;

	public function __construct()
	{
		$this->tb = DB::table('users_temp');
	}

	public function get($params = array())
	{
		if (isset($params['confirm_code'])) {
			$this->tb->where('confirm_code', $params['confirm_code']);
		}

		if (isset($params['email'])) {
			$this->tb->where('email', $params['email']);
		}

		$rs = $this->tb->first();

		if (count($rs) > 0) {
			return $rs;
		}

		return false;

	}

	public function store($params = array())
	{

		$sql = "INSERT INTO users_temp (user_name, email, confirm_code, password, created_at) VALUES ('".$params['user_name']."', '".$params['email']."', '".$params['confirm_code']."', '".$params['password']."', '".$params['created_at']."')  ON DUPLICATE KEY UPDATE confirm_code = '".$params['confirm_code']."'";

		return DB::statement($sql);
	}

	public function delete($email)
	{
		return $this->tb->where('email', $email)->delete();

	}

}
